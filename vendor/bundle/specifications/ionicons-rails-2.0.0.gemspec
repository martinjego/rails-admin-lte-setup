# -*- encoding: utf-8 -*-
# stub: ionicons-rails 2.0.0 ruby lib

Gem::Specification.new do |s|
  s.name = "ionicons-rails"
  s.version = "2.0.0"

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib"]
  s.authors = ["Zhao Han"]
  s.date = "2014-12-13"
  s.homepage = "https://github.com/umhan35/ionicons-rails"
  s.licenses = ["MIT"]
  s.rubygems_version = "2.5.1"
  s.summary = "Ionicons for Ruby on Rails"

  s.installed_by_version = "2.5.1" if s.respond_to? :installed_by_version
end
